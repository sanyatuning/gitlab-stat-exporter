# gitlab-stat-exporter

Fork of: https://github.com/Whyeasy/gitlab-extra-exporter

This is a Prometheus exporter for Gitlab to get information via the API.

Currently, this exporter retrieves the following data:

```
gitlab_issue_count{group="frontend",labels="security,severity:critical",old="2",stale="false",state="opened"} 2
gitlab_merge_request_count{group="frontend",labels="dependencies,javascript,security",old="2",stale="false",state="opened",status="can_be_merged"} 10
```

Labels:
- group: the root gitlab group
- old: age in weeks
- stale: true when not updated in the last 30 days otherwise false
- state: state of the MR or issue (always open for now)
- status: merge status (only for MRs)

## Use

You can run this exporter as a docker container.

Available images: https://gitlab.com/sanyatuning/gitlab-stat-exporter/container_registry

## Configuration

### Required

Provide your Gitlab URI; `--gitlabURI <string>` or as env variable `GITLAB_URI`.

Provide a Gitlab API Key with access to projects and merge requests; `--gitlabAPIKey <string>` or as env variables `GITLAB_API_KEY`

### Optional

Change listening port of the exporter; `--listenAddress <string>` or as env variable `LISTEN_ADDRESS`. Default = `8080`

Change listening path of the exporter; `--listenPath <string>` or as env variable `LISTEN_PATH`. Default = `/metrics`

Change the interval of retrieving data in the background; `--interval <string>` or as env variable `INTERVAL`. Default is `3600`

