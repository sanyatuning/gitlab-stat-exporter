// Package client contains all the files to extract the information from gitlab
package client

import (
	"net/http"
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"

	"gitlab.com/sanyatuning/gitlab-stat-exporter/internal"
)

type ExporterClient struct {
	gitlabURI    string
	gitlabAPIKey string
	httpClient   *http.Client
	interval     time.Duration
}

// New returns a new Client connection to Gitlab.
func New(c internal.Config) *ExporterClient {
	convertedTime, _ := strconv.ParseInt(c.Interval, 10, 64)

	exporter := &ExporterClient{
		gitlabAPIKey: c.GitlabAPIKey,
		gitlabURI:    c.GitlabURI,
		httpClient:   &http.Client{Timeout: 10 * time.Second},
		interval:     time.Duration(convertedTime),
	}

	exporter.startFetchData()

	return exporter
}

func (c *ExporterClient) getData(updatedAfter time.Time) error {
	log.Info("Getting new data.")
	glc, err := gitlab.NewClient(c.gitlabAPIKey, gitlab.WithBaseURL(c.gitlabURI), gitlab.WithHTTPClient(c.httpClient))
	if err != nil {
		return err
	}

	projects, err := getProjects(glc)
	if err != nil {
		return err
	}

	err = getMergeRequest(glc, projects, updatedAfter)
	if err != nil {
		return err
	}

	err = getIssues(glc, projects, updatedAfter)
	if err != nil {
		return err
	}

	log.Info("New data retrieved.")

	return nil
}

func (c *ExporterClient) startFetchData() {
	// Do initial call to have data from the start.
	go func() {
		twoYearAgo := time.Now().AddDate(-2, 0, 0)
		err := c.getData(twoYearAgo)
		if err != nil {
			log.Error("Scraping failed.", err)
		}
	}()

	ticker := time.NewTicker(c.interval * time.Second)
	quit := make(chan struct{})

	go func() {
		for {
			select {
			case <-ticker.C:
				dayAgo := time.Now().AddDate(0, 0, -1)
				err := c.getData(dayAgo)
				if err != nil {
					log.Error("Scraping failed.", err)
				}
			case <-quit:
				ticker.Stop()
				return
			}
		}
	}()
}
