package client

import (
	"strings"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	log "github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
)

var issueCountVector = NewGaugeVec(prometheus.GaugeOpts{
	Name: "gitlab_issue_count",
}, []string{"group", "state", "stale", "old", "labels"})

func getIssues(c *gitlab.Client, projects map[int]string, updatedAfter time.Time) error {
	now := time.Now()
	var issuesTotal []*gitlab.Issue

	page := 1

	for {
		issues, _, err := c.Issues.ListIssues(&gitlab.ListIssuesOptions{
			ListOptions:  gitlab.ListOptions{Page: page, PerPage: 100},
			UpdatedAfter: &updatedAfter,
			State:        gitlab.String("all"),
			Scope:        gitlab.String("all"),
		})
		if err != nil {
			return err
		}

		if len(issues) == 0 {
			break
		}

		issuesTotal = append(issuesTotal, issues...)
		page++
	}

	log.Info("found a total of: ", len(issuesTotal), " issues")

	skipped := 0
	for _, issue := range issuesTotal {
		group := projects[issue.ProjectID]
		if group == "" {
			// skip probably archived project
			skipped++
			continue
		}
		issueCountVector.UpdateStat(issue.ID, prometheus.Labels{
			"group":  group,
			"state":  issue.State,
			"stale":  stale(issue.UpdatedAt, now),
			"old":    weekOld(issue.CreatedAt, now),
			"labels": strings.Join(issue.Labels, ","),
		})
	}
	log.Info("skipped: ", skipped, " issues")

	return nil
}
