package client

import (
	"fmt"
	"strings"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	log "github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
)

var mrCountVector = NewGaugeVec(prometheus.GaugeOpts{
	Name: "gitlab_merge_request_count",
}, []string{"group", "state", "status", "stale", "old", "labels"})

// getMergeRequest retrieves all merge requests
func getMergeRequest(c *gitlab.Client, projects map[int]string, updatedAfter time.Time) error {
	now := time.Now()
	var mrTotal []*gitlab.MergeRequest

	page := 1
	skipped := 0

	for {
		mrs, _, err := c.MergeRequests.ListMergeRequests(&gitlab.ListMergeRequestsOptions{
			ListOptions:  gitlab.ListOptions{Page: page, PerPage: 100},
			UpdatedAfter: &updatedAfter,
			State:        gitlab.String("all"),
			Scope:        gitlab.String("all"),
			WIP:          gitlab.String("no"),
		})
		if err != nil {
			return err
		}

		if len(mrs) == 0 {
			break
		}
		for _, mr := range mrs {
			group := projects[mr.ProjectID]
			if group == "" {
				// skip probably archived project
				skipped++
				continue
			}
			mrCountVector.UpdateStat(mr.ID, prometheus.Labels{
				"group":  group,
				"state":  mr.State,
				"status": mr.DetailedMergeStatus,
				"stale":  stale(mr.UpdatedAt, now),
				"old":    weekOld(mr.CreatedAt, now),
				"labels": strings.Join(mr.Labels, ","),
			})
		}

		mrTotal = append(mrTotal, mrs...)
		page++
	}

	log.Info("found a total of: ", len(mrTotal), " MRs")
	log.Info("skipped: ", skipped, " MRs")

	return nil
}

func weekOld(createdAt *time.Time, now time.Time) string {
	if createdAt == nil {
		return ""
	}
	weeks := now.Sub(*createdAt).Hours() / 24 / 7
	if weeks > 9 {
		return "9+"
	}
	return fmt.Sprintf("%d", int(weeks))
}

func stale(updatedAt *time.Time, now time.Time) string {
	if updatedAt == nil {
		return ""
	}
	if updatedAt.AddDate(0, 1, 0).Before(now) {
		return "true"
	}
	return "false"
}
