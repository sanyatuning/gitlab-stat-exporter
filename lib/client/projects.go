package client

import (
	"strings"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	log "github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
)

var prCountVector = NewGaugeVec(prometheus.GaugeOpts{
	Name: "gitlab_project_count",
}, []string{"group", "default_branch", "features", "stale"})

// getProjectStats retrieves all projects from Gitlab.
func getProjects(c *gitlab.Client) (map[int]string, error) {
	now := time.Now()
	result := make(map[int]string)
	page := 1

	for {
		projects, _, err := c.Projects.ListProjects(&gitlab.ListProjectsOptions{
			ListOptions: gitlab.ListOptions{Page: page, PerPage: 100},
			Archived:    gitlab.Bool(false),
			Simple:      gitlab.Bool(false),
		})
		if err != nil {
			return nil, err
		}

		if len(projects) == 0 {
			break
		}
		for _, project := range projects {
			group := strings.Split(project.PathWithNamespace, "/")[0]
			result[project.ID] = group
			prCountVector.UpdateStat(project.ID, prometheus.Labels{
				"group":          group,
				"default_branch": project.DefaultBranch,
				"features":       features(project),
				"stale":          stale(project.LastActivityAt, now),
			})
		}
		page++
	}

	log.Info("found a total of: ", len(result), " projects")

	return result, nil
}

func features(project *gitlab.Project) string {
	var res []string
	if project.JobsEnabled {
		res = append(res, "ci")
	}
	if project.SharedRunnersEnabled {
		res = append(res, "shared_runners")
	}
	if project.LFSEnabled {
		res = append(res, "lfs")
	}
	if project.IssuesEnabled {
		res = append(res, "issues")
	}
	if project.ContainerRegistryEnabled {
		res = append(res, "registry")
	}
	if project.MergeRequestsEnabled {
		res = append(res, "merge_requests")
	}
	if project.PackagesEnabled {
		res = append(res, "packages")
	}
	if project.SnippetsEnabled {
		res = append(res, "snippets")
	}
	if project.WikiEnabled {
		res = append(res, "wiki")
	}
	return strings.Join(res, ",")
}
