package client

import (
	"reflect"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

type GaugeVec struct {
	oldLabels map[int]prometheus.Labels
	vector    *prometheus.GaugeVec
}

func NewGaugeVec(opts prometheus.GaugeOpts, labels []string) *GaugeVec {
	return &GaugeVec{
		oldLabels: make(map[int]prometheus.Labels),
		vector:    promauto.NewGaugeVec(opts, labels),
	}
}

func (g *GaugeVec) UpdateStat(id int, labels prometheus.Labels) {
	oldLabels := g.oldLabels[id]
	if reflect.DeepEqual(oldLabels, labels) {
		return
	}
	if oldLabels != nil {
		g.vector.With(oldLabels).Sub(1)
	}
	g.vector.With(labels).Add(1)
	g.oldLabels[id] = labels
}
